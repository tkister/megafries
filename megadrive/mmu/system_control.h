/* megafries, a Megadrive emulator
   Copyright 2009 Thomas Kister
   SPDX-License-Identifier: LGPL-3.0-or-later
*/
#ifndef SYSTEM_CONTROL_H_
#define SYSTEM_CONTROL_H_


uint32_t Read_Control( uint32_t Address, void* Data, uint16_t Data_Length);

uint32_t Write_Control( uint32_t Address, void* Data, uint16_t Data_Length);


#endif // SYSTEM_CONTROL_H_
