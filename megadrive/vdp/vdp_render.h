/* megafries, a Megadrive emulator
   Copyright 2009 Thomas Kister
   SPDX-License-Identifier: LGPL-3.0-or-later
*/
#ifndef VDP_RENDER_H_
#define VDP_RENDER_H_


uint32_t Init_Renderer();


#endif // VDP_RENDER_H_
