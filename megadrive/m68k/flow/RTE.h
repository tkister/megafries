/* megafries, a Megadrive emulator
   Copyright 2009 Thomas Kister
   SPDX-License-Identifier: LGPL-3.0-or-later
*/
#ifndef RTE_H_
#define RTE_H_


#include "../commons.h"


void mnemo_RTE( struct M68k_Context* M68k_Context_p, int32_t* N_Ticks);


#endif // RTE_H_
