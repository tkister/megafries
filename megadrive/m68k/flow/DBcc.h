/* megafries, a Megadrive emulator
   Copyright 2009 Thomas Kister
   SPDX-License-Identifier: LGPL-3.0-or-later
*/
#ifndef DBCC_H_
#define DBCC_H_


#include "../commons.h"


void mnemo_DBcc( struct M68k_Context* M68k_Context_p, int32_t* N_Ticks);


#endif // DBCC_H_
