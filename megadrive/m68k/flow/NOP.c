/* megafries, a Megadrive emulator
   Copyright 2009 Thomas Kister
   SPDX-License-Identifier: LGPL-3.0-or-later
*/
#include "NOP.h"


void mnemo_NOP( struct M68k_Context* M68k_Context_p, int32_t* N_Ticks)
{
	*N_Ticks -= 4;
}
