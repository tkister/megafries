/* megafries, a Megadrive emulator
   Copyright 2009 Thomas Kister
   SPDX-License-Identifier: LGPL-3.0-or-later
*/
#ifndef MULU_H_
#define MULU_H_


#include "../commons.h"


void mnemo_MULU( struct M68k_Context* M68k_Context_p, int32_t* N_Ticks);


#endif // MULU_H_
