/* megafries, a Megadrive emulator
   Copyright 2009 Thomas Kister
   SPDX-License-Identifier: LGPL-3.0-or-later
*/
#ifndef NEG_H_
#define NEG_H_


#include "../commons.h"


void mnemo_NEG_B( struct M68k_Context* M68k_Context_p, int32_t* N_Ticks);
void mnemo_NEG_W( struct M68k_Context* M68k_Context_p, int32_t* N_Ticks);
void mnemo_NEG_L( struct M68k_Context* M68k_Context_p, int32_t* N_Ticks);


#endif // NEG_H_
