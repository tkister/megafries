/* megafries, a Megadrive emulator
   Copyright 2009 Thomas Kister
   SPDX-License-Identifier: LGPL-3.0-or-later
*/
#include "SUBX.h"


void mnemo_SUBX_B( struct M68k_Context* M68k_Context_p, int32_t* N_Ticks)
{
	// TODO
	M68k_Context_p->Interruptions |= INT_ILLEGAL;
}

void mnemo_SUBX_W( struct M68k_Context* M68k_Context_p, int32_t* N_Ticks)
{
	// TODO
	M68k_Context_p->Interruptions |= INT_ILLEGAL;
}

void mnemo_SUBX_L( struct M68k_Context* M68k_Context_p, int32_t* N_Ticks)
{
	// TODO
	M68k_Context_p->Interruptions |= INT_ILLEGAL;
}
