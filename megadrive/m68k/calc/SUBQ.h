/* megafries, a Megadrive emulator
   Copyright 2009 Thomas Kister
   SPDX-License-Identifier: LGPL-3.0-or-later
*/
#ifndef SUBQ_H_
#define SUBQ_H_


#include "../commons.h"


void mnemo_SUBQ_B( struct M68k_Context* M68k_Context_p, int32_t* N_Ticks);
void mnemo_SUBQ_W( struct M68k_Context* M68k_Context_p, int32_t* N_Ticks);
void mnemo_SUBQ_L( struct M68k_Context* M68k_Context_p, int32_t* N_Ticks);


#endif // SUBQ_H_
