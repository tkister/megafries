/* megafries, a Megadrive emulator
   Copyright 2009 Thomas Kister
   SPDX-License-Identifier: LGPL-3.0-or-later
*/
#ifndef IET_H_
#define IET_H_


#include <stdint.h>


// Effective Address Calculation Time
extern int32_t EACT_Word[64];
extern int32_t EACT_Long[64];


#endif /*IET_H_*/
