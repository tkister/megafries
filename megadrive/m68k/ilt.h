/* megafries, a Megadrive emulator
   Copyright 2009 Thomas Kister
   SPDX-License-Identifier: LGPL-3.0-or-later
*/
#ifndef ILT_H_
#define ILT_H_


#include "commons.h"


extern Instruction_Method Instruction_Lookup_Table[1024];

void Init_ILT();


#endif /*ILT_H_*/
