/* megafries, a Megadrive emulator
   Copyright 2009 Thomas Kister
   SPDX-License-Identifier: LGPL-3.0-or-later
*/
#ifndef AND_H_
#define AND_H_


#include "../commons.h"


void mnemo_AND_B( struct M68k_Context* M68k_Context_p, int32_t* N_Ticks);
void mnemo_AND_W( struct M68k_Context* M68k_Context_p, int32_t* N_Ticks);
void mnemo_AND_L( struct M68k_Context* M68k_Context_p, int32_t* N_Ticks);


#endif // AND_H_
