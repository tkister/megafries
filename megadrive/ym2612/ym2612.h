/* megafries, a Megadrive emulator
   Copyright 2009 Thomas Kister
   SPDX-License-Identifier: LGPL-3.0-or-later
*/
#ifndef YM2612_H_
#define YM2612_H_


#include <stdint.h>


uint8_t Read_ym2612();


#endif // YM2612_H_
